library(readr)
library(tidyr)
library(dplyr)
library(lubridate)
library(ggplot2)
library(countrycode)
library(ggsci)
library(editheme)
library(shadowtext)
library(ggrepel)
editheme::set_base_sty()

covid = read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv")
i = 100

df = covid %>% select(`Country/Region`, matches("\\d") ) %>%
  gather(key = dia, value = confirmados, -`Country/Region`) %>%
  mutate(dia = lubridate::mdy(dia)) %>% janitor::clean_names() %>%
  group_by(country_region, dia) %>% 
  summarise(total = sum(confirmados)) %>%
  ungroup() %>%
  group_by(country_region) %>%
  filter(total >= i) %>%  
  mutate(dia_d = dia - min(dia))  %>%
  mutate(countr = countrycode(country_region, "country.name", "iso2c")) %>%
  left_join(countrycode::codelist_panel[, c("country.name.en", "continent","region", "eurocontrol_pru")],
            by = c("country_region" = "country.name.en")) 

# latinoamérica --------------------------------------------------------------------------------------
p =  df %>% 
  filter(region == "South America" | country_region == "Mexico") %>%
  ggplot(aes(dia_d, total, color = countr)) +
  geom_line(show.legend = F) + scale_color_d3(palette = "category20") + 
  editheme::theme_editor() +
  theme(panel.grid.major =  element_line(color = "gray60", linetype = "dotted", size = .5),
        panel.grid.minor =  element_line(color = "gray60", linetype = "dotted", size = .2)) +
  geom_shadowtext(data = . %>% filter(dia == Sys.Date()), aes(dia_d, label = countr), show.legend = F, color = "gray80") +
  xlab(sprintf("Días desde el caso número %s confirmado en el país respectivo", i) ) +
  ylab("Total de casos")  

png("covid.png", width = 500, height = 750)
p + labs(caption = "gitlab.com/datamarindo, fuente datos: raw.githubusercontent.com/CSSEGISandData/",
       title = "Evolución de casos confirmados de Covid-19")
dev.off()

png("covid_log.png", width = 500, height = 750)
p + 
  labs(caption = "gitlab.com/datamarindo, fuente datos: raw.githubusercontent.com/CSSEGISandData/",
       title = "Evolución de casos confirmados de Covid-19\nEscala logarítmica") +
  scale_y_log10()
dev.off()

# global--------------------------------------------------------------------------------------------------------------------
p = df %>%
  filter(country_region %in% c("Mexico", "Italy", "US", "Iran", "Germany", "France", "Spain", 
                               "Switzerland", "United Kingdom")) %>%
  ggplot(aes(dia_d, total, color = countr)) +
  geom_line(show.legend = F) + scale_color_d3(palette = "category20") + 
  editheme::theme_editor() +
  theme(panel.grid.major =  element_line(color = "gray60", linetype = "dotted", size = .5),
        panel.grid.minor =  element_line(color = "gray60", linetype = "dotted", size = .2)) +
  geom_shadowtext(data = . %>% filter(dia == Sys.Date()), aes(dia_d, label = country_region), show.legend = F, color = "gray80",
                  hjust = .2) +
  xlab(sprintf("Días desde el caso número %s confirmado en el país respectivo", i) ) +
  ylab("Total de casos")  

png("covid_global.png", width = 500, height = 750)
p + labs(caption = "gitlab.com/datamarindo, fuente datos: raw.githubusercontent.com/CSSEGISandData/",
         title = "Evolución de casos confirmados de Covid-19")
dev.off()

png("covid_log_global.png", width = 500, height = 750)
p + 
  labs(caption = "gitlab.com/datamarindo, fuente datos: raw.githubusercontent.com/CSSEGISandData/",
       title = "Evolución de casos confirmados de Covid-19\nEscala logarítmica") +
  scale_y_log10()
dev.off()
